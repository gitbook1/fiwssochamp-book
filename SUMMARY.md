# Summary

* [Introduction](README.md)
  * [中華碩銓資訊](information.md)
* 工作日誌
  * [0826工作項目](worklogs/0826.md)
  * [0827工作項目](worklogs/0827.md)
  * [0902工作項目](worklogs/0902.md)
  * [0903工作項目](worklogs/0903.md)
  * [090405工作項目](worklogs/090405.md)
  * [090910工作項目](worklogs/090910.md)
  * [091112工作項目](worklogs/091112.md)
* [Laravel框架開發技巧](laravel/README.md)
  * vscode對應安裝外掛 _Laravel Blade Snippets_
  * laravel整合Xdebug
* 資料應用管理
  * [壅塞事件偵測與路段績效](TPES/README.md)
    * [1. 主線路段(以交流道為基礎劃分)交通資料與預設對應車輛偵測器設定](TPES/item1.md)
    * [2. 單位路段(以每公里為基礎劃分)交通資料與預設對應車輛偵測器設定](TPES/item2.md)
    * [3. 出入口匝道相對應車輛偵測器設定](TPES/item3.md):優先已有流程與UI元件，完成後做1.2.
    * [4. 主線壅塞程度評估參數設定](TPES/item4.md):Error
    * [5. 出入口匝道壅塞程度評估參數設定](TPES/item5.md):Error
    * [6. 路段自由車速參數設定](TPES/item6.md):Error
    * [7. 旅行時間壅塞評估參數設定](TPES/item7.md):優先已有流程與UI元件，完成後做4.5.
    * [8. 主線VD 壅塞事件延伸長度設定](TPES/item8.md):Error
  * [旅行時間演算](TTES/README.md)
    * [2. 路段旅行時間權重設定數設定主線](TTES/item2.md)
    * [3. 特定日路段旅行時間權重設定數設定主線](TTES/item3.md)
    * [4. 歷史旅行時間權重設定與特殊日設定](TTES/item4.md)
    * [6. 旅行時間驗證](TTES/item6.md)
*. [車輛資料收集](VDS/README.md)
*. [雍塞回堵偵測收集](QLD/README.md)
*. [eTag偵測設備](ETAG/README.md)
*. [WDS](WDS/README.md)
   *. [雨量資料收集](WDS/rd.md)
   *. [濃霧資料收集](WDS/vi.md)
   *. [風力資訊收集](WDS/wd.md)
   *. [天候資訊](WDS/cwb.md)
*. [影像事件偵測](IID/README.md)
*. [隧道機電](TEE/README.md)