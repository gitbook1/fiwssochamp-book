#特定日路段旅行時間權重設定數設定主線
直接參考[2. 路段旅行時間權重設定數設定主線](TTES/item2.md)流程處理，只是步驟多加一道選擇特殊日期欄位機制。

##TODOs
- [ ] 建立Model class
  - [ ] LocationTtsSpdDistInfo.php / 儲存特殊日(假日)設定
- [ ] 新增功能
  - [ ] 先建立資料範本 => 確認產出數值符合需求
  - [ ] 使用fakedata
- [ ] 修改功能
  - [ ] 欄位對應：先把編號顯示出來，欄位編號
- [ ] 匯入功能
  - [ ] 按鈕UI配置加在修改右側
  - [ ] 可匯入csv處理
  - [ ] **匯入xls處理？**（目前先做完csv部分，xls再確認）


程式檔
app/Http/Controllers/Ttes/MgrSectionttspercentageController.php

讀取資料檔
app/Models/LocationMileInfo.php
app/Models/LocationTtsSpdDistInfo.php (儲存特殊日(假日)設定)

寫入資料檔
**app/Models/LocationTtsDistInfo.php (儲存特殊日(假日)設定)**



**需求**