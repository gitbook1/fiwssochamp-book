# 歷史旅行時間權重設定與特殊日設定

-----
**需求**
- [ ] 新增「歷史旅行時間群組設定」頁面
   1. 對應「HisDateGroupTbl」資料表
   2. 規則
      1. 最多12筆：一月一筆。
      2. 起始與結束去判斷是否次年（結束小於起始），程式判斷。
      3. 設定完成一次出現所有設定群組。
      4. 修改：清除後重建作法。

## TODOs
- [x] 建立app/Models/HisCommonConfig.php並配置連線歷史資料庫history(mysql_history))
  - [x] VD、eTag優先、ETC優先、eTag與ETC兩者平均，此為單一下拉欄位。
- [x] 欠缺spd資料表以及確認時間日期格式
  - [x] **HisSpecDayConfigTbl**
  - [x] HisSpecDayConfigTbl[ Date | reason]

- [x] 程式檔與資料表


程式檔
app/Http/Controllers/Ttes/MgrHtdspdpercentageController.php

讀取資料檔
app/Models/SpdMain.php
app/Models/HisCommonConfig.php
app/Models/HisSpecDayConfigTbl.php

寫入資料檔
**app/Models/HisCommonConfig.php**
**app/Models/HisSpecDayConfigTbl.php**