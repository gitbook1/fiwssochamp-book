# 旅行時間驗證

------
**規範**
真實旅行時間：Htd5MinOriSecEtcDatTbl
推估旅行時間：Htd5MinSecTravelDataTbl

## 待確認問題
- [x] 如何開發？先去看「第16629章 雲端化中央電腦系統軟體_v5」內，確認後與旅行時間演算軟體內一致。
  - [x] 確認現況畫面問題，主要去做「Htd5MinSecTravelDataTbl」推估來源數據與「Htd5MinOriSecEtcData」實際數據比較
  - [ ] [小柯]會確認是否欄位有遺漏。



程式檔
app/Http/Controllers/Ttes/MgrSectimecompetctimeController.php

讀取資料檔
app/Models/LocationMileInfo.php
app/Models/Htd5MinSecTravelData.php
app/Models/Htd5MinOriSecEtcData.php
