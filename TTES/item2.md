#路段旅行時間權重設定數設定主線

##TODOs
- [x] 產生csv匯入檔案(DataTable既有功能)，根據匯出資料進行
  ~~- [ ] 修正將設備名稱改成路段編號？~~
  - [x] js樹狀按鈕點選觸發載入機制程式片段在哪？
點選樹狀結構對應會觸發事件機制
```javascript
// resources/views/qld/mgr_vdjam/index.blade.php
function zTreeInit() {
    FIWS.setting['treeMenu']= {
        // 是否開啟多選checkbox
        check:{
            // enable:false
        },
        // 點選後對應事件
        checkChange:function(treeId,cNodes) {
            var aNodes=FIWS.obj[treeId].getCheckedNodes(true);
            FIWS.obj['dataTable'].clear();
            $.each(aNodes,function(keys,vals) {
                if (vals.isdata != true) return;
                $.each(dbCache.datas['dataTable'].Rows,function(index,val) {
                    if (vals.name==val.rampOutLocationId) {
                        FIWS.obj['dataTable'].row.add(val);
                    }
                });
            });
            FIWS.obj['dataTable'].draw(); //更新dataTable
        },
    };
}
```
  - [x] js修改完成的notify訊息
```javascript
// 還有info跟error可以用
toastr.success('成功');
toastr.warning(e.errMsg);
```
  - [ ] index()進入後初始化ajax步驟？

- [x] 建立Model class
  - [x] LocationTtsDistInfo.php / 儲存一般設定
- [ ] 建檔/新增功能
  - [ ] 偵測點選路段有無資料
    - [ ] 無，列表顯示預設值上方功能顯示「新增」
    - [ ] 有，列表載入實際數值上方功能顯示「修改」
- [x] 匯入功能
  - [x] 利用匯出欄位調整可接受csv形式
  - [x] 按鈕UI配置加在FIWS.init dataTable內
  - [x] 可匯入csv處理
  - [ ] **匯入xls處理？**（目前先做完csv部分，xls再確認）
- [x] 修改功能
  - [x] 欄位對應：先把編號顯示出來，欄位編號


程式檔
app/Http/Controllers/Ttes/MgrSectionttspercentageController.php

讀取資料檔
app/Models/LocationMileInfo.php
**app/Models/LocationTtsDistInfo.php (尚無本Model class)**

寫入資料檔
**app/Models/LocationTtsDistInfo.php (尚無本Model class)**



**需求**
1. LocationMileInfoTbl提供樹狀選單結構，包含設備資訊，從LocationMileInfoTbl取出locationType=1的locationId與locationName欄位清單
2. 當使用者點選路段，從後端show()取得dataTable內容
3.

**開發規範**
```javascript
// 看到以下註解可能代表使用產生假資料
/*檢驗用 E*/
```

**開發規範**
在app/Http/Controllers/Tpes/MgrVdlocationController.php有**show()的呼叫範本**
```php
//取登入者的資料
$user=Session::get('user');

// vardump
dd();

// 在blade.php檔案內dump/使用helper函式語法
{{-- {{ dd($fun_name2) }} --}}
```


```php
// laravel 在Model內直接使用 DB instance 不需use

// Controller內呼叫取得table數據
$res = \DB::table('LocationMileInfoTbl')
                    ->where('locationType', '1')
                    ->where('controlCenterId', $controlCenterId)
                    ->where('freewayId', $tmp[0])
                    ->where('mainDirection', $tmp[2] )
                    ->orderBy('freewayId', 'asc')
                    ->orderBy('mainDirection', 'asc')
                    ->orderBy('fmileAge', 'asc')
                    ->limit(50)
                    ->get();

// from /app/Models/LocationTtsDistInfo.php
$row = \DB::connection($this->connection)->select("
        SELECT
          A.controlCenterId, A.locationType, A.freewayId,
          A.mainDirection, A.uniqueId, A.locationName,
          B.name AS freewayName,
          IF(A.fmileAge <= A.tmileAge, (fmileAge), (0-fmileAge)) AS Ageby
        FROM LocationMileInfoTbl AS A
        LEFT JOIN SysCodeTbl AS B ON B.type='0002' AND B.code=A.freewayId
        WHERE 1=1 {$where}
        ORDER BY
          A.controlCenterId ASC, A.locationType ASC, A.freewayId ASC, A.mainDirection ASC,
          Ageby ASC
      ");
```




- [ ] 關於 3. 特定日路段旅行時間權重設定數設定主線
關於SpdMainTbl此表配置直接取出於畫面給使用者點選（下拉式選單）
```
CREATE TABLE `SpdMainTbl` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '索引',
  `spdName` varchar(50) NOT NULL,
  `spdYear` int(4) NOT NULL COMMENT '年度',
  `spdDate` date NOT NULL DEFAULT '1970-01-01' COMMENT '特定日日期',
  `spdList` text NOT NULL COMMENT '連續假日',
  PRIMARY KEY (`id`),
  UNIQUE KEY `SpdMainIdx` (`spdDate`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
```