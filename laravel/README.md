#laravel
本案使用Laravel Framework 5.6.39

----

## 使用POST與Token範本
laravel預設已將csrf機制加在route內的hook過程中，所以必須要在呼叫$.ajax({post})
```javascript
// resources/views/ttes/mgr_sectionttspercentage/index.blade.php
// line294
headers:{ 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
```
```php
// 使用blade方式
{{ csft_token() }}
```

## 使用Factory與Seeder透過Faker函式庫建立測試資料集
- [x] 使用Fakedata處理建立特定日旅行時間權重表
  - [x] 在database/seeds/建立新的DatabaseSeeder.php

###建立Factory
```bash
$ php artisan make:factory LocationTtsSpdDistInfoFactory
Factory created successfully.
```
產生
```php
<?php
use Faker\Generator as Faker;
$factory->define(Model::class, function (Faker $faker) {
    return [
        //
    ];
});

```

```bash
$ php artisan make:factory LoTtsSpdDistInfoFactory --model=LocationTtsSpdDistInfo
Factory created successfully.
```
產生
```php
<?php
use Faker\Generator as Faker;
$factory->define(App\LocationTtsSpdDistInfo::class, function (Faker $faker) {
    return [
        //
    ];
});

```

###建立Seeder
```bash
$php artisan make:seeder LocationTtsSpdDistInfoSeeder
```

* [Laravel 台灣翻譯文件 | Laravel 道場](https://docs.laravel-dojo.com/laravel/5.5/database-testing)
* [PHP Laravel 5 : database seeding with ‘faker’ - Chutipong Roobklom - Medium](https://medium.com/@khunemz/php-laravel-5-database-seeding-with-faker-c7dcce5dabe2)
* [fzaninotto/Faker: Faker is a PHP library that generates fake data for you](https://github.com/fzaninotto/Faker)

