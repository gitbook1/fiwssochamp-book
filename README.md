# 整合式工作站軟體FIWS

-------
## 專案參與資訊
環境參數
web伺服器配置與帳號
<http://192.168.2.108:9002/>
內建gitlab環境帳號
<http://192.168.2.112/>
VPN管理
<https://59.125.102.185:10443/>
san / *******

本機開發環境與VPS都使用帳號密碼
  

lavavel版本與MySQL
IBM-MQ：主資料庫
IBM-Redis：收進來的資訊會先進Redis內經過處理後刪除

[中華碩銓資訊](information.md)

---
相關資料檔備份於 <san.personalwork@gmail.com> 雲端硬碟 中華碩銓_FIWS 目錄

---
## 領域名詞解釋

VD車輛偵測器

CMS佈設位置坐標
[tdata.thb.gov.tw/thbOd/browse/cms/info](http://tdata.thb.gov.tw/thbOd/browse/cms/info)

TDCS(Traffic Data Collection System)國道高速公路電子收費交通資料蒐集支援系統使用手冊

###旅⾏行行時間演算軟體
旅行時間驗證？
	是指要開發驗證演算法還是要通過一個既有的驗證規則？

附錄 - 旅行時間演算軟體
使用資料庫

- IB(MQ)
  - 主要資料交換與動態資料庫
- IB(Redis) => Redis
  - 用來處理歷史靜態資料

-------
## 負責開發項目

###[資料應用管理=旅行時間演算](TTES/README.md)
對應文件 **附錄 - 旅行時間演算軟體：先處理**
* 管理功能
  [2. 路段旅行時間權重設定數設定主線](TTES/item2.md)
  [3. 特定日路段旅行時間權重設定數設定主線](TTES/item3.md)
  ~~[4. 歷史旅行時間權重設定與特殊日設定](TTES/item4.md)~~
  ~~5. 路徑旅行時間建構表設定~~
  [6. 旅行時間驗證](TTES/item6.md)

* 操作功能
  * [5分鐘旅行時間查詢](TTES/itemlog1.md)
  * [歷史旅行時間分類查詢](TTES/itemlog2.md)
  * [旅行時間即時監視](TTES/itemlog3.md)


###[資料應用管理=壅塞事件偵測與路段績效](TPES/README.md)
對應文件 **附錄 - 壅塞偵測與交通績效評估軟體**
* 管理功能
  [1. 主線路段(以交流道為基礎劃分)交通資料與預設對應車輛偵測器設定](TPES/item1.md)
  [2. 單位路段(以每公里為基礎劃分)交通資料與預設對應車輛偵測器設定](TPES/item2.md)
  [3. 出入口匝道相對應車輛偵測器設定](TPES/item3.md)
  [4. 主線壅塞程度評估參數設定](TPES/item4.md)
  [5. 出入口匝道壅塞程度評估參數設定](TPES/item5.md)
  [6. 路段自由車速參數設定](TPES/item6.md)
  [7. 旅行時間壅塞評估參數設定](TPES/item7.md)
  [8. 主線VD 壅塞事件延伸長度設定](TPES/item8.md)
* 操作功能
  [1. 壅塞偵測與交通績效評估](TPES/itemlog1.md)
  [2. 主線壅塞程度評估參數報表](TPES/itemlog2.md)
  [3. 出入口匝道壅塞程度評估參數報表](TPES/itemlog3.md)
  [4. 旅行時間壅塞程度評估參數報表](TPES/itemlog4.md)
  [5. 不同來源績效比較](TPES/itemlog5.md)
