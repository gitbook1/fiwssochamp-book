#主線路段(以交流道為基礎劃分)交通資料與預設對應車輛偵測器設定



## TODOs
- [ ] 確認設定table是否有排序值，若無預設排序值直接依照id排序
- [ ] 排序機制作法範本
- [ ] 確認操作與儲存紀錄

程式檔
app/Http/Controllers/`Tpes/MgrVdlocation`Controller.php

讀取資料檔
app/Models/VdConfig.php
app/Models/LocationMileInfo.php

寫入資料檔
**app/Models/LocationVdConfig.php**

**需求**
1. 可以考慮去除分頁機制
2. 第一步驟選擇後先出現設備清單，設備清單直接在點選可以針對該設定設定排序值。